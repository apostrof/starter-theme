<?php
$context = Timber::get_context();
$context['layout'] = 'layout.twig';
$context['layout_sidebar'] = 'layout-sidebar.twig';
$context['post'] = Timber::query_post(false, 'IRD_Campaign');
$context['post']->archive_link = get_post_type_archive_link( 'apo_campaign' );
$context['post']->tags = $context['post']->terms('post_tag');
$context['next'] = get_next_post();
$context['prev'] = get_adjacent_post(false, '', true);
Timber::render(['single-campaign.twig'], $context);
