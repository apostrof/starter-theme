<?php

$context = Timber::get_context();
$paged = get_query_var('paged');
$posts = new Timber\PostQuery([
    'post_type' => 'apo_campaign',
    'paged'     => $paged
], 'IRD_Campaign');
$pagination = Timber::get_pagination([]);

// var_dump($posts);

$context['layout'] = 'layout.twig';
$context['layout_sidebar'] = 'layout-sidebar.twig';
$context['posts'] = $posts;
$context['pagination'] = $pagination;

Timber::render(['campaigns.twig'], $context);
