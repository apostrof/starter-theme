<?php
$context = Timber::get_context();
$context['layout'] = 'layout.twig';
$context['layout_sidebar'] = 'layout-sidebar.twig';
$context['page'] = new Timber\Post();

Timber::render(['page.twig'], $context);
