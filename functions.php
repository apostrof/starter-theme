<?php
require_once(__DIR__ . '/vendor/autoload.php');

new \Ent\Ent([
    'theme_dir' => __DIR__,
    'post_class_map' => [
        'post'          => 'IRD_Post',
        'apo_campaign'  => 'IRD_Campaign',
        'apo_event'     => 'IRD_Event',
        'apo_worker'    => 'IRD_Worker',
        'apo_clipping'  => 'IRD_Clipping',
    ],
    'menus' => [
        'main_menu' => __('Menú Principal', 'apostrof'),
    ],
    'sidebars' => [
        'footer_first'  => __('Primer peu', 'apostrof'),
        'footer_second' => __('Segón peu', 'apostrof'),
        'footer_third'  => __('Tercer peu', 'apostrof'),
        'footer_fourth'  => __('Quart peu', 'apostrof'),
        'sidebar'       => __('Barra lateral', 'apostrof'),
    ],
    'google_maps_api_key' => 'AIzaSyAb8HqH92nAEf0ML095xzlUDOZ_vDokb8E',
]);