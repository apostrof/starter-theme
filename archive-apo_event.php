<?php

$context = Timber::get_context();
$page = new Timber\Post();
$paged = get_query_var('paged');
$is_archive = get_query_var('archive');
$posts = new Timber\PostQuery([
    'post_type' => 'apo_event',
    'paged'     => $paged,
    'posts_per_page' => 9,
    'order' => $is_archive ? 'DESC' : 'ASC',
    'orderby' => 'meta_value',
    'meta_key' => '_start_date',
    'meta_value'   => date( "Y-m-d" ),
    'meta_compare' => $is_archive ? '<' : '>=',
], 'IRD_Event');
$pagination = $posts->pagination();

// var_dump($page);

$context['layout'] = 'layout.twig';
$context['layout_sidebar'] = 'layout-sidebar.twig';
$context['page'] = $page;
$context['posts'] = $posts;
$context['pagination'] = $pagination;
$context['is_archive'] = $is_archive;
$context['archive_url'] = get_post_type_archive_link('apo_event');

Timber::render(['agenda.twig'], $context);
