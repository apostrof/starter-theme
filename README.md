# starter-theme

WordPress theme template for Ent.

## Installation

Run `composer install` and `npm install`

## Compilation

Watch: `gulp`
Build: `gulp build` 