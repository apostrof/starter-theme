<?php
use \Ent\VisualComposer\Helpers;

class WPBakeryShortCode_apostrof_marquee extends Ent\VisualComposer\ShortCode {
    protected function getContextData(array $atts) {
        return [];
    }
}
$admin_tpl = <<<TPL
    <h4 style="color: #00CB8B">{{{ params.text || Marquesina }}}</h4>
TPL;

Helpers::map([
    'base' => 'apostrof_marquee',
    'name' => 'Marquesina — Apòstrof',
    'category' => 'Apòstrof',
    'custom_markup' => $admin_tpl,
    'params' => [
        [
            'type'       => 'textfield',
            'heading'    => __('Text', 'apostrof'),
            'param_name' => 'text',
        ],
    ]
]);