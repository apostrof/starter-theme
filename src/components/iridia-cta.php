<?php
use \Ent\VisualComposer\Helpers;

class WPBakeryShortCode_apostrof_cta extends Ent\VisualComposer\ShortCode {
    protected function getContextData(array $atts) {
        return [];
    }
}
$admin_tpl = <<<TPL
    <style>
        .apo-cta {
            text-align: center;
        }

        .apo-cta h5 {
            line-height: 1.3em !important;
            margin-top: 0;
        }

        .apo-cta--text a {
            color: #00CB8B
        }

        .apo-cta--box {
            padding: 15px;
            background-color: #00CB8B;
            max-width: 350px;
            margin: 0 auto;
        }
        
        .apo-cta--box a {
            padding: 8px 10px;
            border: 2px solid black;
            background-color: #00CB8B;
            text-transform: uppercase;
            display: inline-block;
            color: black;
            text-decoration: none;
            margin-top: 10px;
        }
    
    </style>
    <# var link = params.link.split('|') #>
    <div class="apo-cta apo-cta--{{{ params.format }}}">
        <# if ( params.format === 'text' ) { #>
            <h5>{{{ params.text }}}<a href="{{{ link[0].split(':')[1] }}}"> {{{ link[1].split(':')[1] }}}</a></h5>    
        <# } else { #>
            <h5>{{{ params.text }}}</h5>
            <a href="{{{ link[0].split(':')[1] }}}"> {{{ decodeURIComponent(link[1].split(':')[1]) }}}</a>
        <# } #>
    </div>
TPL;

Helpers::map([
    'base' => 'apostrof_cta',
    'name' => 'CTA — Apòstrof',
    'category' => 'Apòstrof',
    'custom_markup' => $admin_tpl,
    'params' => [
        [
            'type'       => 'textfield',
            'heading'    => __('Text', 'apostrof'),
            'param_name' => 'text',
        ],
        [
            'type'       => 'vc_link',
            'heading'    => __('Enllaç', 'apostrof'),
            'param_name' => 'link',
        ],
        [
            'type'       => 'dropdown',
            'heading'    => __('Format', 'apostrof'),
            'param_name' => 'format',
            'value' => [
                'Paràgraf' => 'text',
                'Caixa' => 'box',
            ]
        ],
    ]
]);