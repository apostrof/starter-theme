<?php
use \Ent\VisualComposer\Helpers;

class WPBakeryShortCode_apostrof_section_title extends Ent\VisualComposer\ShortCode {
    protected function getContextData(array $atts) {
        return [];
    }
}

$admin_tpl = <<<TPL
    <hr style="border-top: 0; border-bottom: 3px solid #00CB8B">
    <h3>{{{ params.heading }}}</h3>
TPL;

Helpers::map([
    'base' => 'apostrof_section_title',
    'name' => 'Títol secció — Apòstrof',
    'category' => 'Apòstrof',    
    'custom_markup' => $admin_tpl,
    'params' => [
        [
            'type'       => 'textfield',
            'heading'    => __('Títol', 'apostrof'),
            'param_name' => 'heading',
        ],
    ]
]);