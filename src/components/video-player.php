<?php
use \Ent\VisualComposer\Helpers;

class WPBakeryShortCode_apostrof_video_slider extends Ent\VisualComposer\ShortCode {
    protected function getContextData(array $atts) {
        return [];
    }
}
$admin_tpl = <<<TPL
    <style>


        </style>
    <div class="apo-video-slider">
        <video width="100" height="60" controls>
            <source src="" type="video/mp4">
            <source src="" type="video/ogg">
            Your browser does not support the video tag.
        </video>
    </div>
TPL;

Helpers::map([
    'base' => 'apostrof_video_slider',
    'name' => 'Video Slider — Apòstrof',
    'category' => 'Apòstrof',
    'custom_markup' => $admin_tpl,
    'params' => [
        [
            'type'       => 'attach_image',
            'heading'    => __('Video', 'apostrof'),
            'param_name' => 'video',
        ],
    ]
]);