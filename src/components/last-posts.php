<?php
use \Ent\VisualComposer\Helpers;

class WPBakeryShortCode_apostrof_last_posts extends Ent\VisualComposer\ShortCode {
    protected function getContextData(array $atts) {
        $multiplier = $atts['show_more_posts'] ? 2 : 1;
        $posts = new Timber\PostQuery([
            'post_type' => [$atts['post_type']],
            'posts_per_page' => $atts['posts_count'] * $multiplier
        ]);
        return [
            'posts' => $posts
        ];
    }
}
$admin_tpl = <<<TPL
    <div class="apo-last-posts">
        <h4>Darreres entrades: {{{ params.post_type }}}</h4>
    </div>
TPL;

Helpers::map([
    'base' => 'apostrof_last_posts',
    'name' => 'Darreres entrades — Apòstrof',
    'category' => 'Apòstrof',    
    'custom_markup' => $admin_tpl,
    'params' => [
        [
            'type'       => 'posttypes',
            'heading'    => __('Tipus de post', 'apostrof'),
            'param_name' => 'post_type',
        ],
        [
            'type'       => 'dropdown',
            'heading'    => __('Nombre d\'entrades', 'apostrof'),
            'param_name' => 'posts_count',
            'value'      => ['3','6','9']
        ],
        [
            'type'       => 'checkbox',
            'heading'    => __('Entrades adicionals', 'apostrof'),
            'description' => 'Si es selecciona aquesta opció es s\'inclourà un botó per mostrar més entrades',
            'param_name' => 'show_more_posts'
        ],
    ]
]);