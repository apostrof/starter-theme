<?php
use \Ent\VisualComposer\Helpers;

class WPBakeryShortCode_apostrof_section_list extends Ent\VisualComposer\ShortCode {
    protected function getContextData(array $atts) {
        return [];
    }
}
$admin_tpl = <<<TPL
    <style>
        .apo-section__list ul {
            list-style: none;
            margin-left: 90px !important;
        }
        .apo-section__list ul li{
            list-style: none !important;
            position: relative;
        }
        .apo-section__list ul li:before{
            content: '';
            display: block;
            position: absolute;
            left: -60px;
            top: 10px;
            width: 30px;
            height: 3px;
            background-color: #00CB8B;
        }
    </style>
    <div class="apo-section__list">
        {{{ params.content }}}
    </div>
TPL;

Helpers::map([
    'base' => 'apostrof_section_list',
    'name' => 'Llista — Apòstrof',
    'category' => 'Apòstrof',    
    'custom_markup' => $admin_tpl,
    'params' => [
        [
            'type'       => 'textarea_html',
            'heading'    => __('Títol', 'apostrof'),
            'param_name' => 'content',
        ],
    ]
]);