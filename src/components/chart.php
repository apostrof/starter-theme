<?php
use \Ent\VisualComposer\Helpers;

class WPBakeryShortCode_apostrof_chart extends Ent\VisualComposer\ShortCode {
    protected function getContextData(array $atts) {
        $labels = '[';
        $values = '[';
        $bg_colors = '[';
        foreach (explode(',', $atts['sections']) as $section) {
            $section = explode('/', $section);
            $labels .= '\'' . trim($section[0]) . '\',';
            $values .= trim($section[1]) . ',';
            $bg_colors .= '[\'' . trim($section[2]) . '\',\'' . trim($section[3]) . '\'],';
        }
        return [
            'labels' => substr($labels, 0, -1) . ']',
            'values' => substr($values, 0, -1) . ']',
            'bg_colors' => substr($bg_colors, 0, -1) . ']',
        ];
    }
}
$admin_tpl = <<<TPL
    <div class="apo-chart">
        <em>Gràfica pastís</em>
    </div>
TPL;

Helpers::map([
    'base' => 'apostrof_chart',
    'name' => 'Gràfic pastís — Apòstrof',
    'category' => 'Apòstrof',    
    'custom_markup' => $admin_tpl,
    'icon' => 'fas fa-chart-pie',
    'params' => [
        [
            'type'       => 'textfield',
            'heading'    => __('Identificador', 'apostrof'),
            'param_name' => 'id',
        ],
        [
            'type'       => 'exploded_textarea',
            'heading'    => __('Seccions', 'apostrof'),
            'description'    => __('Cada linia es una secció del pastís amb el format \'Nom de la secció: valor/color(hex)/opacitat\' ', 'apostrof'),
            'param_name' => 'sections',
            'value'      => 'Secció / 25/ #000000 /1,
                             Secció 2 / 50 / #414141 / 100,
                             Secció 3 / 25 / #747374 / 96'
        ],
        [
            'type'       => 'textfield',
            'heading'    => __('Unitat', 'apostrof'),
            'param_name' => 'unit',
            'value'      => 'percentatges (%)'
        ],
        [
            'type'       => 'colorpicker',
            'heading'    => __('Color vorera', 'apostrof'),
            'param_name' => 'border_color',
            'value'      => 'rgba(242,242,242,1)'
        ],
        [
            'type'       => 'textfield',
            'heading'    => __('Amplada vorera', 'apostrof'),
            'param_name' => 'border_width',
            'value'      => '2'
        ],
    ]
]);