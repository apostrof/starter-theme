<?php
use \Ent\VisualComposer\Helpers;

class WPBakeryShortCode_apostrof_quote extends Ent\VisualComposer\ShortCode {
    protected function getContextData(array $atts) {
        return [];
    }
}

$admin_tpl = <<<TPL
    <p>“{{{ params.quote }}}” <strong>{{{ params.author }}}</strong></p>
TPL;

Helpers::map([
    'base' => 'apostrof_quote',
    'name' => 'Cita — Apòstrof',
    'category' => 'Apòstrof',
    'custom_markup' => $admin_tpl,
    'params' => [
        [
            'type'       => 'textfield',
            'heading'    => __('Autor', 'apostrof'),
            'param_name' => 'author',
        ],
        [
            'type'       => 'textarea',
            'heading'    => __('Cita', 'apostrof'),
            'param_name' => 'quote',
        ],
    ]
]);