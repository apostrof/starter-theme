<?php
use \Ent\VisualComposer\Helpers;

class WPBakeryShortCode_apostrof_highlight extends Ent\VisualComposer\ShortCode {
    protected function getContextData(array $atts) {
        return [];
    }
}
$admin_tpl = <<<TPL
    <style>
        .apo-highlight {
            background-color:  #000;
            padding: 15px;
        }

        .apo-highlight--light {
            background-color:  #fff;
            padding: 15px;
        }
        
        .apo-highlight h5 {
            color: #00CB8B;
            font-size: 16px !important;
            font-weight: normal;
            margin-top: 0;
            margin-bottom: 10px;
        }

        
        .apo-highlight h6 {
            color: #fff;
            font-size: 20px !important;
            line-height: 1.3em !important;
            margin: 0;
            font-weight: normal;
            text-decoration-color: #00CB8B;
        }
        
        .apo-highlight--light h6 {
            color:  #000;
        }
        
        .apo-highlight h6 a {
            display: inline-block;
            text-decoration: underline;
            color: white;
        }

        .apo-highlight a {
            color:  #fff;
        }

        .apo-highlight--light a {
            color:  #9B9B9B;
        }

        </style>
    <div class="apo-highlight apo-highlight--{{{params.theme}}}">
        <# if (params.title) { #><h5>{{{ params.title }}}</h5><# } #>  
        <h6 style="text-decoration-line: {{{ params.underline ? 'underline' : 'none' }}}">{{{ params.content }}}</h6>
        <# if (params.link) { #>
            <# var link = params.link.split('|') #>
            <a href="{{{ link[0].split(':')[1] }}}"> {{{ decodeURIComponent(link[1].split(':')[1]) }}}</a>
        <# } #>  
    </div>
TPL;

Helpers::map([
    'base' => 'apostrof_highlight',
    'name' => 'Destacat — Apòstrof',
    'category' => 'Apòstrof',
    'custom_markup' => $admin_tpl,
    'params' => [
        [
            'type'       => 'textfield',
            'heading'    => __('Títol', 'apostrof'),
            'param_name' => 'title',
        ],
        [
            'type'       => 'textarea_html',
            'heading'    => __('Text', 'apostrof'),
            'param_name' => 'content',
        ],
        [
            'type'       => 'checkbox',
            'heading'    => __('Subratllar text', 'apostrof'),
            'param_name' => 'underline',
        ],
        [
            'type'       => 'checkbox',
            'heading'    => __('Text gran', 'apostrof'),
            'param_name' => 'large',
        ],
        [
            'type'       => 'vc_link',
            'heading'    => __('Enllaç', 'apostrof'),
            'param_name' => 'link',
        ],
        [
            'type'       => 'dropdown',
            'heading'    => __('Tema', 'apostrof'),
            'param_name' => 'theme',
            'value'      => [
                'Fosc' => 'dark',
                'Clar' => 'light',
                'Imatge' => 'image',
            ]
        ],
        [
            'type'       => 'attach_image',
            'heading'    => __('Imatge de fons', 'apostrof'),
            'param_name' => 'bg_image',
            'dependency' => [
                'element' => 'theme',
                'value'   => 'image'
            ]
        ],
    ]
]);