<?php
// use Ent\Helpers;
// use Carbon_Fields\Container;
// use Carbon_Fields\Field;

// add_action('init', function () {
//     $labels = [
//         'name'          => __('Equip', 'apostrof'),
//         'singular_name' => __('Membre', 'apostrof'),
//         'add_new_item'  => __('Afegeix un nou membre', 'apostrof'),
//     ];
//     register_post_type('apo_worker', [
//         'labels'        => $labels,
//         'menu_icon'     => 'dashicons-groups',
//         'rewrite'       => ['slug' => 'equip'],
//         'public'        => true,
//         'has_archive'   => true,
//         'menu_position' => 8
//     ]);

//     add_post_type_support('apo_worker', ['thumbnail']);
//     remove_post_type_support( 'apo_worker', 'editor' );

// });

// Helpers::setMeta('apo_worker', function () {
//     return Container::make('post_meta', 'Formulari membre')
//         ->show_on_post_type('apo_worker')
//         ->set_context( 'carbon_fields_after_title' )
//         ->set_priority('high')
//         ->add_fields([
//             Field::make('text', 'email', __('Correu electrònic', 'apostrof'))->set_width(50),
//             Field::make('text', 'phone', __('Telèfon de contacte', 'apostrof'))->set_width(50),
//             Field::make('textarea', 'description', __('Descripció', 'apostrof'))->set_rows( 4 ),
//         ]);
// });

// class IRD_Worker extends \Timber\Post {
//     public function __construct($pid = null) {
//         parent::__construct($pid);
//         Helpers::getPostMeta('apo_worker', $this);
//     }
// }