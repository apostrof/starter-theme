<?php
use Ent\Helpers;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('init', function () {
    $labels = [
        'name'          => __('Campanyes', 'apostrof'),
        'singular_name' => __('Campanya', 'apostrof'),
        'add_new_item'  => __('Afegeix una nova campanya', 'apostrof'),
    ];
    register_post_type('apo_campaign', [
        'labels'        => $labels,
        'taxonomies'    => ['post_tag'],
        'menu_icon'     => 'dashicons-megaphone',
        'rewrite'       => ['slug' => 'campanyes', 'with_front' => false],
        'public'        => true,
        'has_archive'   => true,
        'menu_position' => 6,
    ]);

    add_post_type_support('apo_campaign', ['thumbnail']);
});

Helpers::setMeta('apo_campaign', function () {
    return Container::make('post_meta', 'Formulari campanya')
        ->show_on_post_type('apo_campaign')
        ->set_context( 'carbon_fields_after_title' )
        ->set_priority('high')
        ->add_fields([
            Field::make('image', 'featured_video', __('Video principal', 'apostrof'))->set_type( 'video' )->set_value_type( 'url' ),
            Field::make('rich_text', 'goals', __('Objectius', 'apostrof')),
        ]);
});

Helpers::enableVCFor('apo_campaign');

class IRD_Campaign extends \Timber\Post {
    public function __construct($pid = null) {
        parent::__construct($pid);
        Helpers::getPostMeta('apo_campaign', $this);
    }

    public function relatedPosts() {
        return Timber::get_posts([
            'post_type' => 'apo_campaign'
        ], 'IRD_Campaign');
    }
}