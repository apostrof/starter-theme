<?php
use Ent\Helpers;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('init', function () {
    $labels = [
        'name'          => __('Agenda', 'apostrof'),
        'singular_name' => __('Acte', 'apostrof'),
        'add_new_item'  => __('Afegeix un nou acte', 'apostrof'),
    ];
    register_post_type('apo_event', [
        'labels'        => $labels,
        'menu_icon'     => 'dashicons-calendar-alt',
        'rewrite'       => ['slug' => 'agenda'],
        'public'        => true,
        'has_archive'   => true,
        'menu_position' => 7
    ]);

    add_post_type_support('apo_event', ['thumbnail']);
    
});
  
add_filter( 'query_vars', function ( $vars ){
    $vars[] = "archive";
    return $vars;
} );

Helpers::setMeta('apo_event', function () {
    return Container::make('post_meta', 'Formulari acte')
        ->show_on_post_type('apo_event')
        ->set_context( 'carbon_fields_after_title' )
        ->set_priority('high')
        ->add_fields([
            Field::make('date', 'start_date', __('Data d\'inici', 'apostrof'))->set_width(50)->set_input_format( 'd-m-Y', 'd-m-Y' ),
            Field::make('date', 'end_date', __('Data d\'acabament', 'apostrof'))->set_width(50)->set_input_format( 'd-m-Y', 'd-m-Y' ),
            Field::make('map', 'location', __('Lloc de l\'acte', 'apostrof'))->set_position( '41.392177', '2.173961', 14 ),
        ]);
});

class IRD_Event extends \Timber\Post {
    public function __construct($pid = null) {
        parent::__construct($pid);
        Helpers::getPostMeta('apo_event', $this);
        $this->location = carbon_get_post_meta($this->id, 'location');
    }
}