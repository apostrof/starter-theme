<?php
use \Ent\Helpers;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('init', function () {
    Helpers::adminRemoveComments();
    Helpers::adminChangePostLabels([
        'name'     => 'Notícies',
        'singular' => 'Notícia',
        'add'      => 'Afegeix',
    ]);
});

class IRD_Post extends \Timber\Post {
    public function __construct($pid = null) {
        parent::__construct($pid);
        // Helpers::getPostMeta('post', $this);
    }
}
