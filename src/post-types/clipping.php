<?php
use Ent\Helpers;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('init', function () {
    $labels = [
        'name'          => __('Premsa', 'apostrof'),
        'singular_name' => __('Retall', 'apostrof'),
        'add_new_item'  => __('Afegeix un nou retall', 'apostrof'),
    ];
    register_post_type('apo_clipping', [
        'labels'        => $labels,
        'menu_icon'     => 'dashicons-welcome-widgets-menus',
        'rewrite'       => ['slug' => 'premsa'],
        'public'        => true,
        'has_archive'   => true,
        'menu_position' => 9
    ]);

    // add_post_type_support('apo_clipping', ['thumbnail']);
    remove_post_type_support( 'apo_clipping', 'editor' );
    
});

Helpers::setMeta('apo_clipping', function () {
    return Container::make('post_meta', 'Formulari acte')
        ->show_on_post_type('apo_clipping')
        ->set_context( 'carbon_fields_after_title' )
        ->set_priority('high')
        ->add_fields([
            Field::make('date', 'pub_date', __('Data de publicació', 'apostrof'))->set_width(33.33)->set_input_format( 'd-m-Y', 'd-m-Y' ),
            Field::make('text', 'publication', __('Mitjà', 'apostrof'))->set_width(33.33),
            Field::make('text', 'author', __('Autor/a', 'apostrof'))->set_width(33.33),
            Field::make('text', 'clipping_link', __('Enllaç al retall', 'apostrof'))->set_width(100),
            Field::make('checkbox', 'has_document', __('Incloure document?', 'apostrof'))->set_option_value( 'yes' ),
            Field::make('image', 'document', __('Document', 'apostrof'))
                ->set_type('application/pdf')
                ->set_value_type('url')
                ->set_help_text(__('En format pdf', 'apostrof'))
                ->set_conditional_logic( [
                    [
                        'field' => 'has_document',
                        'value' => true,
                    ]
                ]),
        ]);
});

class IRD_Clipping extends \Timber\Post {
    public function __construct($pid = null) {
        parent::__construct($pid);
        Helpers::getPostMeta('apo_clipping', $this);
    }
}