<?php
use Ent\Helpers;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('init', function () {
    $labels = [
        'name'          => __('Línies de treball','apostrof'),
        'singular_name' => __('Línia','apostrof'),
        'search_items'  => __('Cerca línies','apostrof'),
        'all_items'     => __('Totes les línies','apostrof'),
        'add_new_item'  => __('Afegeix una nova línia','apostrof'),
    ];

    register_taxonomy('apo_work_lines', ['apo_worker'], [
        'labels'            => $labels,
        'rewrite'           => ['slug' => 'linia'],
        'public'            => true,
        'show_tagcloud'     => true,
        'show_admin_column' => true,
        'hierarchical'      => true,
        'has_archive'       => true,
        'query_var'         => 'work_line'
    ]);
});

// Helpers::setMeta('apo_work_lines', function () {
//     return Container::make('term_meta', 'Formulari servei')
//         ->show_on_taxonomy('apo_work_lines')
//         ->add_fields(array(
//             Field::make('text', 'class'),
//         ));
// });
    
class IRD_Work_Area extends \Timber\Term {
    public function __construct($tid = null) {
        parent::__construct($tid);
        // Helpers::getTermMeta('apo_work_lines', $this);
    }
}
