<?php
use Ent\Helpers;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('init', function () {
    $labels = [
        'name'          => __('Àrees de treball','apostrof'),
        'singular_name' => __('Àrea','apostrof'),
        'search_items'  => __('Cerca àrees','apostrof'),
        'all_items'     => __('Totes les àrees','apostrof'),
        'add_new_item'  => __('Afegeix una nova àrea','apostrof'),
    ];

    register_taxonomy('apo_work_areas', ['apo_worker'], [
        'labels'            => $labels,
        'rewrite'           => ['slug' => 'area'],
        'public'            => true,
        'show_tagcloud'     => true,
        'show_admin_column' => true,
        'hierarchical'      => true,
        'has_archive'       => true,
        'query_var'         => 'work_area'
    ]);
});

// Helpers::setMeta('apo_work_areas', function () {
//     return Container::make('term_meta', 'Formulari servei')
//         ->show_on_taxonomy('apo_work_areas')
//         ->add_fields(array(
//             Field::make('text', 'class'),
//         ));
// });
    
class IRD_Work_Line extends \Timber\Term {
    public function __construct($tid = null) {
        parent::__construct($tid);
        // Helpers::getTermMeta('apo_work_areas', $this);
    }
}
