import $ from 'jquery';

$('.apo-last-posts').each(function(){
    const box = $(this);
    const button = box.find('[data-show-more]').first();
    const hiddenCells = box.find('.additional-cell');

    button.on('click', function(){
        if (button.hasClass('all-cells-visible')) {
            button.removeClass('all-cells-visible');
            hiddenCells.slideUp(500);
        } else {
            button.addClass('all-cells-visible');
            hiddenCells.slideDown(500);
        }
    });

});