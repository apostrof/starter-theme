import $ from 'jquery';
import Chart from 'chart.js';

$('[data-pie-chart]').each(function(){
    const canvas = $(this);
    const labels = JSON.parse(canvas.data('labels').replace(/'/g,'"'));
    const values = canvas.data('values');
    const bgColors = JSON.parse(canvas.data('bg-colors').replace(/'/g,'"'));
    const myChart = new Chart(this, {
        type: 'pie',
        data: {
            labels: labels,
            datasets: [{
                label: canvas.data('unit'),
                data: values,
                backgroundColor: bgColors.map(color => convertHex(color[0], color[1] | 100)),
                borderColor: canvas.data('border-color'),
                borderWidth: canvas.data('border-width')
            }]
        },
        options: {
            // layout: {
            //     padding: {
            //         bottom: 60
            //     }
            // },
            legend: {
                // display: false,
                position: 'bottom',
                // fullWidth: false,
                labels: {
                    fontSize: 20,
                    fontColor: 'black',
                    // boxWidth: 100,
                    // padding: 60,
                    fontFamily: "'Px Grotesk', 'Liberation Mono', Courier, monospace",
                }
            }
        }
    }).resize();
});

function convertHex(hex,opacity){
    hex = hex.replace('#','');
    const r = parseInt(hex.substring(0,2), 16);
    const g = parseInt(hex.substring(2,4), 16);
    const b = parseInt(hex.substring(4,6), 16);

    return 'rgba('+r+','+g+','+b+','+opacity/100+')';
}