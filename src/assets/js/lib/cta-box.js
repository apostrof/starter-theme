import $ from 'jquery';
import 'jquery-ui/ui/widgets/draggable';

$('.ird-cta--box').each(function(){
    var box = $(this);
    box.draggable({})
    box.find('.ird-cta__close').on('click', function(){
        box.hide();
    })
})