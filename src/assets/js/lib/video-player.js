import $ from 'jquery';

$(document).ready(function(){
    $('[data-video-player]').each(function() {
    
        const container = $(this);
        const player = container.find('video').first()[0];
        const pauseBtn = container.find('[data-video-pause]').first();
        const muteBtn = container.find('[data-video-mute]').first();
    
        const playPromise = player.play();

        if (playPromise !== undefined) {
            playPromise.then(_ => {
                // Automatic playback started!
                // Show playing UI.
                // We can now safely pause video...
                pauseBtn.on('click', function(){
                    if(container.hasClass('is-playing')) {
                        player.pause();
                    } else {
                        player.play();
                    }
                    container.toggleClass('is-playing');
                })
            
                muteBtn.on('click', function(){
                    player.muted = !player.muted;
                    container.toggleClass('with-sound');
                })
            })
            .catch(error => {
                // Auto-play was prevented
                // Show paused UI.
                console.error(error);
            });
        }
    
    });
})