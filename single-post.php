<?php
$context = Timber::get_context();
$post = Timber::query_post(false, 'IRD_Post');
$post->archive_link = get_post_type_archive_link( 'post' );
$post->tags = $post->terms('post_tag');

$context['layout'] = 'layout.twig';
$context['layout_sidebar'] = 'layout-sidebar.twig';
$context['post'] = $post;
// $context['next'] = get_next_post();
// $context['prev'] = get_adjacent_post(false, '', true);
// var_dump($post);
Timber::render(['single-post.twig'], $context);
