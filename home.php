<?php

$context = Timber::get_context();
$paged = get_query_var('paged');
// $posts = Timber::get_posts(['paged' => $paged], 'IRD_Post');
$pagination = Timber::get_pagination([]);

// var_dump($posts);

$context['page'] = new Timber\Post();
$context['layout'] = 'layout.twig';
$context['layout_sidebar'] = 'layout-sidebar.twig';
// $context['posts'] = $posts;
$context['pagination'] = $pagination;

Timber::render(['blog.twig'], $context);
