<?php

$context = Timber::get_context();
$paged = get_query_var('paged');
$posts = new Timber\PostQuery([
    'post_type' => 'apo_clipping',
    'paged'     => $paged,
    'posts_per_page' => 9
], 'IRD_Clipping');
$pagination = Timber::get_pagination([]);

// var_dump($posts);

$context['layout'] = 'layout.twig';
$context['layout_sidebar'] = 'layout-sidebar.twig';
$context['posts'] = $posts;
$context['pagination'] = $pagination;

Timber::render(['press.twig'], $context);
